using System;

class exercise5
{
   static void Main()
   {
      double code, amount;
      double result = 0;

      code = double.Parse(Console.ReadLine());
      amount = double.Parse(Console.ReadLine());

      switch (code)
      {
         case 1 : result = 4.0 * amount;
                break;

         case 2 : result = 4.50 * amount;
                break;

         case 3 : result = 5.0 * amount;
                  break;

         case 4 : result = 2.0 * amount;
                  break;

         case 5 : result = 1.50 * amount;
                  break;
      }

      Console.WriteLine($"Total: R$ {result.ToString("F2")}");
   }
}
