using System;

class exercise5
{
   static void Main()
   {
      double salary;
      int increasePercent;

      salary = double.Parse(Console.ReadLine());

      if (salary < 0)
      {
         return;
      }

      else
      {
         if (salary > 0 && salary <= 400)
         {
            increasePercent = 15;
         }

         else if (salary >= 400.01 && salary < 800)
         {
            increasePercent = 12;
         }

         else if (salary >= 800.01 && salary < 1200)
         {
            increasePercent = 10;
         }

         else if (salary >= 1200.01 && salary <= 2000)
         {
            increasePercent = 7;
         }

         else
         {
            increasePercent = 4;
         }
      }

      double newSalary = (salary + (salary * increasePercent / 100));
      double earned = salary * increasePercent / 100;

      Console.WriteLine($"Novo salario: {newSalary.ToString("F2")}");
      Console.WriteLine($"Reajuste ganho: {earned.ToString("F2")}");
      Console.WriteLine($"Em percentual: {increasePercent} %");
   }
}
