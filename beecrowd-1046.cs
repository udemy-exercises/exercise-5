using System;

class exercise5
{
   static void Main()
   {
      int firstNum, secondNum, iterations, result;

      firstNum = int.Parse(Console.ReadLine());
      secondNum = int.Parse(Console.ReadLine());
      
      if (firstNum == secondNum)
      {
         result = 24;
      }
      
      else
      {
         iterations = 0;
         for (int i = firstNum; ; i++) {
            if (i == 24) {
               i = 0;
            }

            if (i == secondNum) {
               result = iterations;
               break;
            }
            
            iterations++;
         }
      }

      Console.WriteLine($"O JOGO DUROU {result} HORA(S)");
   }
}
