using System;

class exercise5
{
   static void Main()
   {
      double firstNum, secondNum, result;

      firstNum = int.Parse(Console.ReadLine());
      secondNum = int.Parse(Console.ReadLine());
      result = 0;

      if (firstNum > secondNum)
      {
         result = firstNum / secondNum;
      }

      else if (secondNum > firstNum)
      {
         result = secondNum / firstNum;
      }

      if (result % 1 == 0)
      {
         Console.WriteLine("Sao Multiplos");
      }

      else
      {
         Console.WriteLine("Nao sao Multiplos");
      }
   }
}
